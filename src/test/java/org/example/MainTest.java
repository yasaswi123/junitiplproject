package org.example;
import java.util.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    Main main=new Main();
    String fileName1="/home/yasaswi/IdeaProjects/junitiplproject/src/matches.csv";
    String fileName2="/home/yasaswi/IdeaProjects/junitiplproject/src/deliveries.csv";
    List<String[]> matches=main.readFile(fileName1);
    List<String[]> deliveries=main.readFile(fileName2);

    @Test
    void problem1() {
        HashMap<String,Integer> expectedResult=new HashMap<>();
        expectedResult.put("2008",58);
        expectedResult.put("2009",57);
        expectedResult.put("2010",60);
        expectedResult.put("2011",73);
        expectedResult.put("2012",74);
        expectedResult.put("2013",76);
        expectedResult.put("2014",60);
        expectedResult.put("2015",59);
        expectedResult.put("2016",60);
        expectedResult.put("2017",59);
        assertEquals(expectedResult,main.problem1(matches));
        assertNotNull(main.problem1(matches));
    }
    @Test
    void problem2(){
        HashMap<String,Integer> expectedWinners=new HashMap<>();
        expectedWinners.put("Mumbai Indians",92);
        expectedWinners.put("Sunrisers Hyderabad",42);
        expectedWinners.put("Pune Warriors",12);
        expectedWinners.put("Rajasthan Royals",63);
        expectedWinners.put("Kolkata Knight Riders",77);
        expectedWinners.put("Royal Challengers Bangalore",73);
        expectedWinners.put("Gujarat Lions",13);
        expectedWinners.put("Rising Pune Supergiant",10);
        expectedWinners.put("Kochi Tuskers Kerala",6);
        expectedWinners.put("Kings XI Punjab",70);
        expectedWinners.put("Deccan Chargers",29);
        expectedWinners.put("Delhi Daredevils",62);
        expectedWinners.put("Rising Pune Supergiants",5);
        expectedWinners.put("Chennai Super Kings",79);
        assertEquals(expectedWinners,main.problem2(matches));
        assertTrue(main.problem2(matches).size()>0);
        assertNotNull(main.problem2(matches).size());
    }
    @Test
    void problem3(){
        HashMap<String,Integer> expectedExtraruns=new HashMap<>();
        expectedExtraruns.put("Gujarat Lions",98);
        expectedExtraruns.put("Mumbai Indians", 102);
        expectedExtraruns.put("Sunrisers Hyderabad",107);
        expectedExtraruns.put("Kings XI Punjab",100);
        expectedExtraruns.put("Delhi Daredevils", 106);
        expectedExtraruns.put("Rising Pune Supergiants",108);
        expectedExtraruns.put("Kolkata Knight Riders", 122);
        expectedExtraruns.put("Royal Challengers Bangalore",156);
        assertNotNull(main.problem3(matches,deliveries));
        assertSame(expectedExtraruns,main.problem3(matches,deliveries));
        assertFalse(main.problem3(matches,deliveries).size()>0);
    }
    @Test
    void problem4(){
        HashMap<String,Double> expectedEconomicalbowlers=new HashMap<>();
        expectedEconomicalbowlers.put("RN ten Doeschate",3.4285714285714284);
        expectedEconomicalbowlers.put("J Yadav", 4.142857142857142);
        expectedEconomicalbowlers.put("V Kohli",5.454545454545454);
        expectedEconomicalbowlers.put("R Ashwin", 5.7250000000000005);
        expectedEconomicalbowlers.put("S Nadeem",5.863636363636363);
        expectedEconomicalbowlers.put("Z Khan", 6.15483870967742);
        expectedEconomicalbowlers.put("Parvez Rasool",6.200000000000001);
        expectedEconomicalbowlers.put("MC Henriques",6.267515923566879);
        expectedEconomicalbowlers.put("MA Starc",6.75);
        expectedEconomicalbowlers.put("M de Lange",6.9230769230769225);
        assertNotEquals(expectedEconomicalbowlers,main.problem4(matches,deliveries));
        assertEquals(expectedEconomicalbowlers,main.problem4(matches,deliveries));
    }
    @Test
    void problem5(){
        HashMap<String,Integer> expected=new HashMap<>();
        expected.put("DA Warner",4);
        assertEquals(expected,main.problem5(matches));
        assertNotNull(main.problem5(matches));
    }
}